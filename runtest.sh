#!/bin/bash
set -x

if [ ! -e /tmp/qecore_setup_done ]; then
    dnf -y erase python3-behave python2-behave
    sudo pip3 install qecore
    touch /tmp/qecore_setup_done
fi

sudo -u test dogtail-run-headless-next --force-xorg "behave -t $1 -k -f html -o /tmp/report_$TEST.html -f plain"; rc=$?
if [ $rc -eq 1 ]; then
  sudo -u test dogtail-run-headless-next --force-xorg "behave -t $1 -k -f html -o /tmp/report_$TEST.html -f plain"; rc=$?
fi

RESULT="FAIL"
if [ $rc -eq 0 ]; then
  RESULT="PASS"
fi

rhts-report-result $TEST $RESULT "/tmp/report_$TEST.html"

exit $rc
