@entry_feature
Feature: Zenity Entry tests


  @entry_general_title
  Scenario: Entry general Title
    * Start application "zenity" via command "zenity --entry --title=PIRATE_ENTRY"
    * Item "PIRATE_ENTRY" "dialog" is "showing" in "zenity"


  @entry_general_size
  Scenario: Entry set size of the widget
    * Start application "zenity" via command "zenity --entry --width=350 --height=350"
    * Verify height and width of the widget


  @entry_general_rename_ok_button
  Scenario: Entry rename OK button
    * Start application "zenity" via command "zenity --entry --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @entry_general_rename_cancel_button
  Scenario: Entry rename Cancel button
    * Start application "zenity" via command "zenity --entry --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @entry_general_add_extra_button
  Scenario: Entry add Extra button
    * Start application "zenity" via command "zenity --entry --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --entry --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @entry_general_modal
  Scenario: Entry modal
    * Start application "zenity" via command "zenity --entry --modal"
    * Application "zenity" is running


  @entry_general_ok
  Scenario: Entry OK
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY"
    * Left click "OK" "push button"
    * Application "zenity" is no longer running
    * Result is equal to "PIRATE_ENTRY"


  @entry_general_cancel
  Scenario: Entry Cancel
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY"
    * Wait 1 seconds before action
    * Left click "Cancel" "push button"
    * Application "zenity" is no longer running
    * Result is not equal to "ENTRY"


  @entry_general_escape
  Scenario: Entry Escape
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY"
    * Wait 1 seconds before action
    * Press key: "Esc"
    * Application "zenity" is no longer running
    * Result is not equal to "ENTRY"


  @entry_general_timeout
  Scenario: Entry Timeout
    * Start application "zenity" via command "zenity --entry --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @entry
  Scenario: Entry
    * Start application "zenity" via command "zenity --entry"
    * Item "Enter new text:" "label" is "showing" in "zenity"
    * Left click "OK" "push button"
    * Result is equal to "NONE"


  @entry_text
  Scenario: Entry Text
    * Start application "zenity" via command "zenity --entry --text=PIRATE%TEST"
    * Item "PIRATE%TEST" "label" is "showing" in "zenity"
    * Left click "OK" "push button"
    * Result is equal to "NONE"


  @entry_entry_text
  Scenario: Entry Entry-Text
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY"
    * Item "Enter new text:" "label" is "showing" in "zenity"
    * Item "None" "text" has text "PIRATE_ENTRY" in "zenity"
    * Left click "OK" "push button"
    * Result is equal to "PIRATE_ENTRY"


  @entry_entry_text_text
  Scenario: Entry Entry-Text Text
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY --text=PIRATE_FLAG"
    * Item "None" "text" has text "PIRATE_ENTRY" in "zenity"
    * Left click "OK" "push button"
    * Result is equal to "PIRATE_ENTRY"


  @entry_entry_text_hidden
  Scenario: Entry Entry-Text Hidden
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY --hide-text"
    * Item "None" "password text" does not have text "PIRATE_ENTRY" in "zenity"
    * Left click "OK" "push button"
    * Result is equal to "PIRATE_ENTRY"


  @entry_entry_text_text_hidden
  Scenario: Entry Entry-Text Text
    * Start application "zenity" via command "zenity --entry --entry-text=PIRATE_ENTRY --text=PIRATE_FLAG --hide-text"
    * Item "None" "password text" does not have text "PIRATE_ENTRY" in "zenity"
    * Left click "OK" "push button"
    * Result is equal to "PIRATE_ENTRY"
