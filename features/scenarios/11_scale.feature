@scale_feature
Feature: Zenity Scale tests


  @scale_general_title
  Scenario: Scale set title
    * Start application "zenity" via command "zenity --scale --title=PIRATE_SCALE"
    * Item "PIRATE_SCALE" "dialog" is "showing" in "zenity"


  @rhbz1788854
  @scale_general_size
  Scenario: Scale set size of the widget
    * Start application "zenity" via command "zenity --scale --width=350 --height=350"
    * Verify height and width of the widget


  @scale_general_rename_ok_button
  Scenario: Scale rename OK button
    * Start application "zenity" via command "zenity --scale --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @scale_general_rename_cancel_button
  Scenario: Scale rename Cancel button
    * Start application "zenity" via command "zenity --scale --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @scale_general_add_extra_button
  Scenario: Scale add Extra button
    * Start application "zenity" via command "zenity --scale --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --scale --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @scale_general_modal
  Scenario: Scale modal
    * Start application "zenity" via command "zenity --scale --modal"
    * Application "zenity" is running


  @scale_general_ok
  Scenario: Scale OK
    * Start application "zenity" via command "zenity --scale --value=0"
    * Wait 1 second before action
    * Left click "OK" "push button" in "zenity"
    * Result is equal to "0"


  @scale_general_cancel
  Scenario: Scale Cancel
    * Start application "zenity" via command "zenity --scale"
    * Wait 1 second before action
    * Left click "Cancel" "push button" in "zenity"
    * Result is not equal to "0"


  @scale_general_escape
  Scenario: Scale close by Esc key
    * Start application "zenity" via command "zenity --scale"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is not equal to "0"


  @scale_general_timeout
  Scenario: Scale Timeout
    * Start application "zenity" via command "zenity --scale --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @scale_general_move_slider
  Scenario: Scale move_slider
    * Start application "zenity" via command "zenity --scale --value=0"
    * Wait 1 second before action
    * Left click "None" "slider"
    * Left click "OK" "push button"
    * Result is not equal to "0"


  @gate
  @scale
  Scenario: Scale
    * Start application "zenity" via command "zenity --scale"
    * Item "Adjust the scale value" "label" is "showing" in "zenity"


  @scale_text
  Scenario: Scale Text
    * Start application "zenity" via command "zenity --scale --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"


  @scale_value
  Scenario: Scale Value
    * Start application "zenity" via command "zenity --scale --value=55"
    * Item "None" "slider" with description "55" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "55"


  @scale_min_value
  Scenario: Scale Min-Value
    * Start application "zenity" via command "zenity --scale --min-value=-55 --value=-55"
    * Item "None" "slider" with description "-55" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"


  @scale_max_value
  Scenario: Scale Max-Value
    * Start application "zenity" via command "zenity --scale --max-value=155 --value=155"
    * Item "None" "slider" with description "55" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_hide_value
  Scenario: Scale Hide-Value
    * Start application "zenity" via command "zenity --scale --hide-value"
    * Item "None" "slider" does not have description "0" that is "visible" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "0"


  @scale_text_value
  Scenario: Scale Text Value
    * Start application "zenity" via command "zenity --scale --text=PIRATE_TEST --value=55"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "55"


  @scale_text_min_value
  Scenario: Scale Text Min-Value
    * Start application "zenity" via command "zenity --scale --text=PIRATE_TEST --min-value=-55 --value=-55"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"


  @scale_text_max_value
  Scenario: Scale Text Max-Value
    * Start application "zenity" via command "zenity --scale --text=PIRATE_TEST --max-value=155 --value=155"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_text_hide_value
  Scenario: Scale Text Hide-Value
    * Start application "zenity" via command "zenity --scale --hide-value --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Item "None" "slider" does not have description "0" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "0"


  @scale_value_hide_value
  Scenario: Scale Value Hide-Value
    * Start application "zenity" via command "zenity --scale --hide-value --value=55"
    * Item "None" "slider" does not have description "55" in "zenity"
    * Wait 2 seconds before action
    * Left click "OK" "push button" in "zenity"
    * Wait 2 seconds before action
    * Compare result with expected values: "55"


  @scale_min_value_max_value
  Scenario: Scale Min-Max-Value
    * Start application "zenity" via command "zenity --scale --min-value=-55 --max-value=155 --value=55"
    * Wait 2 seconds before action
    * Drag the slider to the "beggining"
    * Item "None" "slider" with description "55" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"
    * Start application "zenity" via command "zenity --scale --min-value=-55 --max-value=155 --value=55"
    * Wait 2 seconds before action
    * Drag the slider to the "end"
    * Item "None" "slider" with description "155" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_min_value_hide_value
  Scenario: Scale Hide-Value
    * Start application "zenity" via command "zenity --scale --min-value=-55 --hide-value --value=-55"
    * Item "None" "slider" does not have description "-55" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"


  @scale_max_value_hide_value
  Scenario: Scale Hide-Value
    * Start application "zenity" via command "zenity --scale --max-value=155 --hide-value --value=155"
    * Item "None" "slider" does not have description "155" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_text_min_value_max_value
  Scenario: Scale Text Min-Max-Value
    * Start application "zenity" via command "zenity --scale --min-value=-55 --max-value=155 --value=55 --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Wait 2 seconds before action
    * Drag the slider to the "beggining"
    * Item "None" "slider" with description "-55" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"
    * Start application "zenity" via command "zenity --scale --min-value=-55 --max-value=155 --value=55 --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Wait 2 seconds before action
    * Drag the slider to the "end"
    * Item "None" "slider" with description "155" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_text_min_value_hide_value
  Scenario: Scale Text Min-Value Hide-Value
    * Start application "zenity" via command "zenity --scale --min-value=-55 --hide-value --value=-55 --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Item "None" "slider" does not have description "55" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"


  @scale_text_max_value_hide_value
  Scenario: Scale Text Max-Value Hide-Value
    * Start application "zenity" via command "zenity --scale --max-value=155 --hide-value --value=155 --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Item "None" "slider" does not have description "55" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_text_min_value_max_value_hide_value
  Scenario: Scale Text Min-Max-Value Hide-Value
    * Start application "zenity" via command "zenity --scale --min-value=-55 --max-value=155 --value=55 --text=PIRATE_TEST --hide-value"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Item "None" "slider" does not have description "55" in "zenity"
    * Wait 2 seconds before action
    * Drag the slider to the "beggining"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "-55"
    * Start application "zenity" via command "zenity --scale --min-value=-55 --max-value=155 --value=55 --text=PIRATE_TEST --hide-value"
    * Item "PIRATE_TEST" "label" is "showing" in "zenity"
    * Item "None" "slider" does not have description "55" in "zenity"
    * Wait 2 seconds before action
    * Drag the slider to the "end"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "155"


  @scale_min_value_larger_than_max_value
  Scenario: Scale min-value larger than max-value error message
    * Run zenity scale with min-value larger than max-value and verify error message


  @scale_value_out_of_bounds
  Scenario: Scale min-value larger than max-value error message
    * Run zenity scale value out of bounds and verify error message