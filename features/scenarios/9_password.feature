@password_feature
Feature: Zenity Password tests

  @password_general_title
  Scenario: Password general option set title
    * Start application "zenity" via command "zenity --password --title=PASSWORD"
    * Item "PASSWORD" "dialog" is "showing"


  @password_general_rename_ok_button
  Scenario: Password general option rename OK button
    * Start application "zenity" via command "zenity --password --ok-label='PIRATEOK'"
    * Item "PIRATEOK" "push button" is "showing"


  @password_general_rename_cancel_button
  Scenario: Password general option rename Cancel button
    * Start application "zenity" via command "zenity --password --cancel-label=PIRATECANCEL"
    * Item "PIRATECANCEL" "push button" is "showing"


  @password_general_add_extra_button
  Scenario: Password general option add Extra button
    * Start application "zenity" via command "zenity --password --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing"
    * Start application "zenity" via command "zenity --password --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing"
    * Item "EXTRA2" "push button" is "showing"


  @password_general_modal
  Scenario: Password modal
    * Start application "zenity" via command "zenity --password --modal"
    * Application "zenity" is running


  @password_general_ok
  Scenario: Password OK
    * Start application "zenity" via command "zenity --password"
    * Wait 1 second before action
    * Type text: "PASSWORD"
    * Left click "OK" "push button"
    * Result is equal to "PASSWORD"


  @password_general_cancel
  Scenario: Password Cancel
    * Start application "zenity" via command "zenity --password"
    * Wait 1 second before action
    * Left click "Cancel" "push button"
    * Result is equal to "NONE"


  @password_general_escape
  Scenario: Password close by Esc key
    * Start application "zenity" via command "zenity --password"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @password_general_timeout
  Scenario: Password Timeout
    * Start application "zenity" via command "zenity --password --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @password
  Scenario: Password
    * Start application "zenity" via command "zenity --password"
    * Item "Password:" "label" is "showing" in "zenity"


  @password_username
  Scenario: Password Username
    * Start application "zenity" via command "zenity --password --username"
    * Item "Password:" "label" is "showing" in "zenity"
    * Item "Username:" "label" is "showing" in "zenity"

