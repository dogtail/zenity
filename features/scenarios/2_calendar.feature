@calendar_feature
Feature: Zenity Calendar tests


  @calendar_general_title
  Scenario: Calendar set title
    * Start application "zenity" via command "zenity --calendar --title=PIRATE_CALENDAR"
    * Item "PIRATE_CALENDAR" "dialog" is "showing" in "zenity"


  @calendar_general_size
  Scenario: Calendar set size of the widget
    * Start application "zenity" via command "zenity --calendar --width=350 --height=350"
    * Verify height and width of the widget


  @calendar_general_rename_ok_button
  Scenario: Calendar rename OK button
    * Start application "zenity" via command "zenity --calendar --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @calendar_general_rename_cancel_button
  Scenario: Calendar rename Cancel button
    * Start application "zenity" via command "zenity --calendar --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @calendar_general_add_extra_button
  Scenario: Calendar add Extra button
    * Start application "zenity" via command "zenity --calendar --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --calendar --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @calendar_general_modal
  Scenario: Calendar modal
    * Start application "zenity" via command "zenity --calendar --modal"
    * Application "zenity" is running


  @calendar_general_ok
  Scenario: Calendar OK
    * Start application "zenity" via command "zenity --calendar --day=13 --date-format=%d"
    * Left click "OK" "push button" in "zenity"
    * Result is equal to "13"

  @calendar_general_cancel
  Scenario: Calendar Cancel
    * Start application "zenity" via command "zenity --calendar --day=13 --date-format=%d"
    * Wait 1 second before action
    * Left click "Cancel" "push button" in "zenity"
    * Result is not equal to "13"


  @calendar_general_escape
  Scenario: Calendar close by Esc key
    * Start application "zenity" via command "zenity --calendar --text=PIRATE_FLAG --day=13 --date-format=%d"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is not equal to "13"


  @calendar_general_select_day_by_doubleclick
  Scenario: Calendar select day by double click
    * Start application "zenity" via command "zenity --calendar --date-format=%d"
    * Wait 1 second before action
    * Double click calendar
    * Result is not equal to "NONE"


  @calendar_general_timeout
  Scenario: Calendar Timeout
    * Start application "zenity" via command "zenity --calendar --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @calendar_basic
  Scenario Outline: Calendar basic information
    * Start application "zenity" via command "<parameters>"
    * Item "<name>" "<roleName>" is "showing" in "zenity"
  Examples: Options
    | parameters                                | name            | roleName      |
    | zenity --calendar                         | Calendar        | dialog        |
    | zenity --calendar --text=PIRATE_FLAG      | PIRATE_FLAG     | label         |


  @calendar_multiple_formats
  Scenario Outline: Calendar with multiple formats
    * Start application "zenity" via command "<parameters>"
    * Item "<name>" "<roleName>" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "<expected result>"
  Examples: Options
    | parameters                                                                               | name        | roleName | expected result |
    | zenity --calendar --text=PIRATE_FLAG --day=13 --date-format=%d                           | PIRATE_FLAG | label    | 13              |
    | zenity --calendar --text=PIRATE_FLAG --month=9 --date-format=%m                          | PIRATE_FLAG | label    | 09              |
    | zenity --calendar --text=PIRATE_FLAG --year=2123 --date-format=%Y                        | PIRATE_FLAG | label    | 2123            |
    | zenity --calendar --text=PIRATE_FLAG --day=13 --month=9 --date-format=%d%m               | PIRATE_FLAG | label    | 1309            |
    | zenity --calendar --text=PIRATE_FLAG --day=13 --year=2123 --date-format=%d%Y             | PIRATE_FLAG | label    | 132123          |
    | zenity --calendar --text=PIRATE_FLAG --month=9 --year=2123 --date-format=%m%Y            | PIRATE_FLAG | label    | 092123          |
    | zenity --calendar --text=PIRATE_FLAG --day=13 --month=9 --year=2123 --date-format=%d%m%Y | PIRATE_FLAG | label    | 13092123        |
