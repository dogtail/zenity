@about_feature
Feature: Zenity About Miscellaneous Option


  @gate
  @about_dialog_open
  Scenario: Open About
    * Start application "zenity" via command "zenity --about"
    * Item "About zenity" "dialog" is "showing" in "zenity"


  @about_dialog_pages
  Scenario: View different About pages   
    * Start application "zenity" via command "zenity --about"
    * Left click "About" "radio button"
    * Left click "Credits" "radio button"
    * Left click "License" "radio button"


  @about_dialog_close_via_key
  Scenario: Close about dialog with Escape key
    * Start application "zenity" via command "zenity --about"
    * Press key: "Esc"
    * Application "zenity" is no longer running


  @about_dialog_close_via_button
  Scenario: Close about dialog with push button
    * Start application "zenity" via command "zenity --about"
    * Left click "Close" "push button"
    * Application "zenity" is no longer running
