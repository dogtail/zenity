@forms_feature
Feature: Zenity Forms tests


  @forms_general_title
  Scenario: Forms set title
    * Start application "zenity" via command "zenity --forms --title=PIRATE_FORM"
    * Item "PIRATE_FORM" "dialog" is "showing" in "zenity"


  @calendar_general_size
  Scenario: Calendar set size of the widget
    * Start application "zenity" via command "zenity --forms --width=350 --height=350"
    * Verify height and width of the widget


  @forms_general_rename_ok_button
  Scenario: Forms rename OK button
    * Start application "zenity" via command "zenity --forms --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @forms_general_rename_cancel_button
  Scenario: Forms rename Cancel button
    * Start application "zenity" via command "zenity --forms --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @forms_general_add_extra_button
  Scenario: Forms add Extra button
    * Start application "zenity" via command "zenity --forms --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --forms --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @forms_general_modal
  Scenario: Forms modal
    * Start application "zenity" via command "zenity --forms --modal"
    * Application "zenity" is running


  @forms_general_ok
  Scenario: Forms OK
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --text=PIRATE_FLAG"
    * Type text: "ENTRY_PIRATE"
    * Left click "OK" "push button" in "zenity"
    * Result is equal to "ENTRY_PIRATE"


  @forms_general_cancel
  Scenario: Forms Cancel
    * Start application "zenity" via command "zenity --forms"
    * Wait 1 second before action
    * Left click "Cancel" "push button" in "zenity"
    * Result is equal to "NONE"


  @forms_general_escape
  Scenario: Forms close by Esc key
    * Start application "zenity" via command "zenity --forms"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @forms_general_timeout
  Scenario: Forms Timeout
    * Start application "zenity" via command "zenity --forms --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @forms_basic
  Scenario Outline: Forms basic information
    * Start application "zenity" via command "<parameters>"
    * Item "<name>" "<roleName>" is "showing" in "zenity"
  Examples: Options
    | parameters                             | name         | roleName |
    | zenity --forms                         | Forms dialog | panel    |
    | zenity --forms --add-entry=ENTRY       | ENTRY        | label    |
    | zenity --forms --add-password=PASSWORD | PASSWORD     | label    |
    | zenity --forms --add-calendar=CALENDAR | CALENDAR     | label    |
    | zenity --forms --text=PIRATE_FLAG      | PIRATE_FLAG  | label    |


  @forms_entry_password
  Scenario: Forms Entry Password
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"


  @forms_entry_text
  Scenario: Forms Entry Text
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --text=PIRATE_FLAG"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"


  @forms_password_text
  Scenario: Forms Password Text
    * Start application "zenity" via command "zenity --forms --add-password=PASSWORD --text=PIRATE_FLAG"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"


  @forms_separator
  Scenario: Forms Separator
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --separator=*"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result with expected values: "*"


  @forms_date_format
  Scenario: Forms Date Format
    * Start application "zenity" via command "zenity --forms --add-calendar=CALENDAR --forms-date-format=%d%y%m"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_entry_calendar
  Scenario: Forms Entry Calendar
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-calendar=CALENDAR --forms-date-format=%d%y%m"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_password_calendar
  Scenario: Forms Password Calendar
    * Start application "zenity" via command "zenity --forms --add-password=PASSWORD --add-calendar=CALENDAR --forms-date-format=%d%y%m"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_calendar_text
  Scenario: Forms Calendar Text
    * Start application "zenity" via command "zenity --forms --add-calendar=CALENDAR --text=PIRATE_FLAG --forms-date-format=%d%y%m"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_entry_password_calendar
  Scenario: Forms Entry Password Calendar
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --add-calendar=CALENDAR --forms-date-format=%d%y%m"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_entry_password_text
  Scenario: Forms Entry Password Text
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --text=PIRATE_FLAG"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"


  @forms_entry_calendar_text
  Scenario: Forms Entry Calendar Text
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-calendar=CALENDAR --text=PIRATE_FLAG --forms-date-format=%d%y%m"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_password_calendar_separator
  Scenario: Forms Password Calendar Separator
    * Start application "zenity" via command "zenity --forms --add-password=PASSWORD --add-calendar=CALENDAR --separator=* --forms-date-format=%d%y%m"
    * Type text: "PASSWORD_TEXT"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result with expected values: "*"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_entry_password_calendar_text
  Scenario: Forms Entry Password Calendar Text
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --add-calendar=CALENDAR --text=PIRATE_FLAG --forms-date-format=%d%y%m"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_entry_password_calendar_separator
  Scenario: Forms Entry Password Calendar Separator
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --add-calendar=CALENDAR --separator=* --forms-date-format=%d%y%m"
    * Type text: "ENTRY_PIRATE"
    * Press key: "Tab"
    * Type text: "PASSWORD_TEXT"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result with expected values: "*"
    * Compare result from zenity with result of command: "date +%d%y%m"


  @forms_entry_password_text_separator
  Scenario: Forms Entry Password Text Separator
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --text=PIRATE_FLAG --separator=* --forms-date-format=%d%y%m"
    * Type text: "ENTRY_PIRATE"
    * Press key: "Tab"
    * Type text: "PASSWORD_TEXT"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result with expected values: "*"


  @forms_entry_password_calendar_text_separator
  Scenario: Forms Entry Password Calendar Text Separator
    * Start application "zenity" via command "zenity --forms --add-entry=ENTRY --add-password=PASSWORD --add-calendar=CALENDAR --text=PIRATE_FLAG --separator=* --forms-date-format=%d%y%m"
    * Type text: "ENTRY_PIRATE"
    * Press key: "Tab"
    * Type text: "PASSWORD_TEXT"
    * Item "ENTRY" "label" is "showing" in "zenity"
    * Item "PASSWORD" "label" is "showing" in "zenity"
    * Item "CALENDAR" "label" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Wait 1 second before action
    * Left click "OK" "push button" that is "showing" in "zenity"
    * Compare result with expected values: "*"
    * Compare result from zenity with result of command: "date +%d%y%m"
