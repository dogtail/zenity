@message_feature
Feature: Zenity Message tests


  @message_question_general_title
  Scenario: Message Question general option set title
    * Start application "zenity" via command "zenity --question --title=QUESTION"
    * Item "QUESTION" "dialog" is "showing"


  @message_question_general_rename_ok_button
  Scenario: Message Question general option rename OK button
    * Start application "zenity" via command "zenity --question --ok-label='PIRATEOK'"
    * Item "PIRATEOK" "push button" is "showing"


  @message_question_general_rename_cancel_button
  Scenario: Message Question general option rename Cancel button
    * Start application "zenity" via command "zenity --question --cancel-label=PIRATECANCEL"
    * Item "PIRATECANCEL" "push button" is "showing"


  @message_question_general_add_extra_button
  Scenario: Message Question general option add Extra button
    * Start application "zenity" via command "zenity --question --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing"
    * Start application "zenity" via command "zenity --question --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing"
    * Item "EXTRA2" "push button" is "showing"


  @message_question_general_modal
  Scenario: Message Question modal
    * Start application "zenity" via command "zenity --question --modal"
    * Application "zenity" is running


  @message_question_general_label
  Scenario: Message Question label
    * Start application "zenity" via command "zenity --question"
    * Item "Are you sure you want to proceed?" "label" is "showing"
    * Start application "zenity" via command "zenity --question --text=PIRATETEST"
    * Item "PIRATETEST" "label" is "showing"


  @message_question_ok
  Scenario: Message Question OK
    * Start application "zenity" via command "zenity --question"
    * Item "Are you sure you want to proceed?" "label" is "showing"
    * Left click "Yes" "push button"
    * Application "zenity" is no longer running


  @message_question_general_cancel
  Scenario: Message Question Cancel
    * Start application "zenity" via command "zenity --question"
    * Wait 1 second before action
    * Left click "No" "push button"
    * Result is equal to "NONE"


  @message_question_general_escape
  Scenario: Message Question close by Esc key
    * Start application "zenity" via command "zenity --question"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @message_question_general_timeout
  Scenario: Message Question Timeout
    * Start application "zenity" via command "zenity --question --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running



  @message_warning_general_title
  Scenario: Message Warning general option set title
    * Start application "zenity" via command "zenity --warning --title=WARNING"
    * Item "WARNING" "dialog" is "showing"


  @message_warning_general_rename_ok_button
  Scenario: Message Warning general option rename OK button
    * Start application "zenity" via command "zenity --warning --ok-label=PIRATEOK"
    * Item "PIRATEOK" "push button" is "showing"


  @message_warning_general_add_extra_button
  Scenario: Message Warning general option add Extra button
    * Start application "zenity" via command "zenity --warning --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing"
    * Start application "zenity" via command "zenity --warning --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing"
    * Item "EXTRA2" "push button" is "showing"


  @message_warning_general_modal
  Scenario: Message Warning modal
    * Start application "zenity" via command "zenity --warning --modal"
    * Application "zenity" is running


  @message_warning_general_label
  Scenario: Message Warning label
    * Start application "zenity" via command "zenity --warning"
    * Item "Are you sure you want to proceed?" "label" is "showing"
    * Start application "zenity" via command "zenity --warning --text=PIRATETEST"
    * Item "PIRATETEST" "label" is "showing"


  @message_warning_general_ok
  Scenario: Message Warning OK
    * Start application "zenity" via command "zenity --warning"
    * Item "Are you sure you want to proceed?" "label" is "showing"
    * Left click "OK" "push button"
    * Application "zenity" is no longer running


  @message_warning_general_escape
  Scenario: Message Warning close by Esc key
    * Start application "zenity" via command "zenity --warning"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @message_warning_general_timeout
  Scenario: Message Warning Timeout
    * Start application "zenity" via command "zenity --warning --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running



  @message_info_general_title
  Scenario: Message Info general option set title
    * Start application "zenity" via command "zenity --info --title=INFO"
    * Item "INFO" "dialog" is "showing"


  @message_info_general_rename_ok_button
  Scenario: Message Info general option rename OK button
    * Start application "zenity" via command "zenity --info --ok-label=PIRATEOK"
    * Item "PIRATEOK" "push button" is "showing"


  @message_info_general_add_extra_button
  Scenario: Message Info general option add Extra button
    * Start application "zenity" via command "zenity --info --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing"
    * Start application "zenity" via command "zenity --info --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing"
    * Item "EXTRA2" "push button" is "showing"


  @message_info_general_modal
  Scenario: Message Info modal
    * Start application "zenity" via command "zenity --info --modal"
    * Application "zenity" is running


  @message_info_general_label
  Scenario: Message Info label
    * Start application "zenity" via command "zenity --info"
    * Item "All updates are complete." "label" is "showing"
    * Start application "zenity" via command "zenity --info --text=PIRATETEST"
    * Item "PIRATETEST" "label" is "showing"


  @message_info_general_ok
  Scenario: Message Info OK
    * Start application "zenity" via command "zenity --info"
    * Item "All updates are complete." "label" is "showing"
    * Left click "OK" "push button"
    * Application "zenity" is no longer running


  @message_info_general_escape
  Scenario: Message Info close by Esc key
    * Start application "zenity" via command "zenity --info"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @message_info_general_timeout
  Scenario: Message Info Timeout
    * Start application "zenity" via command "zenity --info --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running



  @message_error_general_title
  Scenario: Message Error general option set title
    * Start application "zenity" via command "zenity --error --title=ERROR"
    * Item "ERROR" "dialog" is "showing"


  @message_error_general_rename_ok_button
  Scenario: Message Error general option rename OK button
    * Start application "zenity" via command "zenity --error --ok-label=PIRATEOK"
    * Item "PIRATEOK" "push button" is "showing"


  @message_error_general_add_extra_button
  Scenario: Message Error general option add Extra button
    * Start application "zenity" via command "zenity --error --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing"
    * Start application "zenity" via command "zenity --error --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing"
    * Item "EXTRA2" "push button" is "showing"


  @message_error_general_modal
  Scenario: Message Error modal
    * Start application "zenity" via command "zenity --error --modal"
    * Application "zenity" is running


  @message_error_general_label
  Scenario: Message Error label
    * Start application "zenity" via command "zenity --error"
    * Item "An error has occurred." "label" is "showing"
    * Start application "zenity" via command "zenity --error --text=PIRATETEST"
    * Item "PIRATETEST" "label" is "showing"


  @message_error_general_ok
  Scenario: Message Error OK
    * Start application "zenity" via command "zenity --error"
    * Item "An error has occurred." "label" is "showing"
    * Left click "OK" "push button"
    * Application "zenity" is no longer running


  @message_error_general_escape
  Scenario: Message Error close by Esc key
    * Start application "zenity" via command "zenity --error"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @message_error_general_timeout
  Scenario: Message Error Timeout
    * Start application "zenity" via command "zenity --error --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running
