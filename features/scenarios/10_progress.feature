@progress_feature
Feature: Zenity Progress tests


  @progress_general_title
  Scenario: Progress general option set title
    * Start application "zenity" via command "zenity --progress --title=PROGRESS"
    * Item "PROGRESS" "dialog" is "showing"


  @progress_general_rename_ok_button
  Scenario: Progress general option rename OK button
    * Start application "zenity" via command "zenity --progress --ok-label='PIRATEOK'"
    * Item "PIRATEOK" "push button" is "showing"


  @progress_general_rename_cancel_button
  Scenario: Progress general option rename Cancel button
    * Start application "zenity" via command "zenity --progress --cancel-label=PIRATECANCEL"
    * Item "PIRATECANCEL" "push button" is "showing"


  @progress_general_add_extra_button
  Scenario: Progress general option add Extra button
    * Start application "zenity" via command "zenity --progress --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing"
    * Start application "zenity" via command "zenity --progress --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing"
    * Item "EXTRA2" "push button" is "showing"


  @progress_general_modal
  Scenario: Progress modal
    * Start application "zenity" via command "zenity --progress --modal"
    * Application "zenity" is running


  @progress_general_cancel
  Scenario: Message Error Cancel
    * Start application "zenity" via command "zenity --progress"
    * Left click "Cancel" "push button"
    * Application "zenity" is no longer running


  @progress_general_no_cancel
  Scenario: Message Error Cancel
    * Start application "zenity" via command "zenity --progress --no-cancel"
    * Item "Cancel" "push button" is not "showing"
    * Application "zenity" is running


  @progress_general_escape
  Scenario: Progress close by Esc key
    * Start application "zenity" via command "zenity --progress"
    * Wait 1 second before action
    * Press key: "Esc"
    * Result is equal to "NONE"


  @progress_general_timeout
  Scenario: Progress Timeout
    * Start application "zenity" via command "zenity --progress --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @progress
  Scenario: Progress
    * Start application "zenity" via command "zenity --progress"
    * Item "Progress" "dialog" is "showing" in "zenity"


  @progress_text
  Scenario: Progress Text
    * Start application "zenity" via command "zenity --progress --text=PIRATE_FLAG"
    * Item "Progress" "dialog" is "showing" in "zenity"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"


  @known_issue
  @progress_percentage
  Scenario: Progress Percentage
    * Make a pipe: "zenity_pipe"
    * Listen to a pipe: "zenity_pipe", with: "zenity --progress --percentage=50"
    * Wait 3 seconds before action
    * Send: "75", to a pipe: "zenity_pipe"
    * Wait 3 seconds before action
    * Send: "100", to a pipe: "zenity_pipe"
    * Left click "OK" "push button" in "zenity"
    * Application "zenity" is no longer running


  @progress_pulsate
  Scenario: Progress Pulsate
    * Make a pipe: "zenity_pipe"
    * Listen to a pipe: "zenity_pipe", with: "zenity --progress --pulsate"
    * Wait 3 seconds before action
    * Send: "100", to a pipe: "zenity_pipe"
    * Left click "OK" "push button" in "zenity"
    * Application "zenity" is no longer running


  @known_issue
  @progress_text_percentage
  Scenario: Progress Text Percentage
    * Make a pipe: "zenity_pipe"
    * Listen to a pipe: "zenity_pipe", with: "zenity --progress --text=PIRATE_FLAG --percentage=50"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Wait 3 seconds before action
    * Send: "75", to a pipe: "zenity_pipe"
    * Wait 3 seconds before action
    * Send: "100", to a pipe: "zenity_pipe"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Left click "OK" "push button" in "zenity"
    * Application "zenity" is no longer running


  @progress_percentage_autoclose
  Scenario: Progress Percentage Auto-close
    * Make a pipe: "zenity_pipe"
    * Listen to a pipe: "zenity_pipe", with: "zenity --progress --auto-close --percentage=50"
    * Wait 3 seconds before action
    * Send: "75", to a pipe: "zenity_pipe"
    * Wait 3 seconds before action
    * Send: "100", to a pipe: "zenity_pipe"
    * Application "zenity" is no longer running


  @known_issue
  @progress_text_percentage_autoclose
  Scenario: Progress Text Percentage Auto-close
    * Make a pipe: "zenity_pipe"
    * Listen to a pipe: "zenity_pipe", with: "zenity --progress --text=PIRATE_FLAG --percentage=50 --auto-close"
    * Item "PIRATE_FLAG" "label" is "showing" in "zenity"
    * Wait 3 seconds before action
    * Send: "75", to a pipe: "zenity_pipe"
    * Wait 3 seconds before action
    * Send: "100", to a pipe: "zenity_pipe"
    * Application "zenity" is no longer running


  @progress_time_remaining
  Scenario: Progress Percentage Auto-close
    * Make a pipe: "zenity_pipe"
    * Listen to a pipe: "zenity_pipe", with: "zenity --progress --percentage=0 --time-remaining"
    * Wait 1 seconds before action
    * Send: "5", to a pipe: "zenity_pipe"
    * Item "Time remaining:" "label" is not "showing"
    * Wait 1 seconds before action
    * Send: "25", to a pipe: "zenity_pipe"
    * Item "Time remaining:" "label" is "showing"
    * Wait 1 seconds before action
    * Send: "50", to a pipe: "zenity_pipe"
    * Item "Time remaining:" "label" is "showing"
    * Wait 1 seconds before action
    * Send: "100", to a pipe: "zenity_pipe"
    * Item "Time remaining:" "label" is not "showing"
