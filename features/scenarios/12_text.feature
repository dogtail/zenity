@textinfo_feature
Feature: Zenity Text-Info tests


  @text_general_title
  Scenario: Text set title
    * Start application "zenity" via command "zenity --text-info --title=PIRATE_TEXT"
    * Item "PIRATE_TEXT" "dialog" is "showing" in "zenity"


  @text_general_size
  Scenario: Text set size of the widget
    * Start application "zenity" via command "zenity --text-info --width=350 --height=350"
    * Verify height and width of the widget


  @text_general_rename_ok_button
  Scenario: Text rename OK button
    * Start application "zenity" via command "zenity --text-info --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @text_general_rename_cancel_button
  Scenario: Text rename Cancel button
    * Start application "zenity" via command "zenity --text-info --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @text_general_add_extra_button
  Scenario: Text add Extra button
    * Start application "zenity" via command "zenity --text-info --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --text-info --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @text_general_modal
  Scenario: Text modal
    * Start application "zenity" via command "zenity --text-info --modal"
    * Application "zenity" is running


  @text_general_ok
  Scenario: Text OK
    * Start application "zenity" via command "zenity --text-info --editable"
    * Wait 1 second before action
    * Key combo: "<Ctrl><A>"
    * Type: "PIRATE_TEXT"
    * Left click "OK" "push button" in "zenity"
    * Result is equal to "PIRATE_TEXT"


  @text_general_cancel
  Scenario: Text Cancel
    * Start application "zenity" via command "zenity --text-info --editable"
    * Wait 1 second before action
    * Key combo: "<Ctrl><A>"
    * Type: "PIRATE_TEXT"
    * Left click "Cancel" "push button" in "zenity"
    * Result is not equal to "PIRATE_TEXT"


  @text_general_escape
  Scenario: Text close by Esc key
    * Start application "zenity" via command "zenity --text-info --editable"
    * Wait 1 second before action
    * Key combo: "<Ctrl><A>"
    * Type: "PIRATE_TEXT"
    * Press key: "Esc"
    * Result is not equal to "PIRATE_TEXT"


  @text_general_timeout
  Scenario: Text Timeout
    * Start application "zenity" via command "zenity --text-info --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @text
  Scenario: Text-Info
    * Start application "zenity" via command "zenity --text-info"
    * Item "Text View" "dialog" is "showing" in "zenity"


  @text_filename
  Scenario: Text-Info Filename
    * Create file "pirate_file.txt" with data "12345"
    * Start application "zenity" via command "zenity --text-info --filename=/tmp/pirate_file.txt"
    * Item "None" "text" has text "12345" in "zenity"
    * Remove created file


  @text_editable
  Scenario: Text-Info Editable
    * Start application "zenity" via command "zenity --text-info --editable"
    * Key combo: "<Ctrl><A>"
    * Type: "67890"
    * Item "None" "text" has text "67890" in "zenity"


  @text_checkbox
  Scenario: Text-Info Checkbox
    * Start application "zenity" via command "zenity --text-info --checkbox=CHECKBOX"
    * Item "CHECKBOX" "check box" is "showing" in "zenity"


  @text_filename_editable
  Scenario: Text-Info Filename Editable
    * Create file "pirate_file.txt" with data "12345"
    * Start application "zenity" via command "zenity --text-info --filename=/tmp/pirate_file.txt --editable"
    * Item "None" "text" has text "12345" in "zenity"
    * Key combo: "<Ctrl><A>"
    * Type text: "67890"
    * Item "None" "text" has text "67890" in "zenity"
    * Remove created file


  @text_filename_checkbox
  Scenario: Text-Info Filename Checkbox
    * Create file "pirate_file.txt" with data "12345"
    * Start application "zenity" via command "zenity --text-info --filename=/tmp/pirate_file.txt --checkbox=CHECKBOX"
    * Item "None" "text" has text "12345" in "zenity"
    * Item "CHECKBOX" "check box" is "showing" in "zenity"
    * Remove created file


  @text_editable_checkbox
  Scenario: Text-Info Editable Checkbox
    * Start application "zenity" via command "zenity --text-info --editable --checkbox=CHECKBOX"
    * Key combo: "<Ctrl><A>"
    * Type text: "67890"
    * Item "CHECKBOX" "check box" is "showing" in "zenity"
    * Item "None" "text" has text "67890" in "zenity"


  @text_editable_checkbox_allows_ok_button
  Scenario: Text-Info Editable Checkbox allows OK button
    * Start application "zenity" via command "zenity --text-info --editable --checkbox=CHECKBOX"
    * Key combo: "<Ctrl><A>"
    * Type: "PIRATE_CHECKBOX"
    * Left click "OK" "push button"
    * Application "zenity" is running
    * Left click "CHECKBOX" "check box" 
    * Left click "OK" "push button"
    * Application "zenity" is no longer running
    * Result is equal to "PIRATE_CHECKBOX"


  @text_filename_editable_checkbox
  Scenario: Text-Info Filename Editable Checkbox
    * Create file "pirate_file.txt" with data "12345"
    * Start application "zenity" via command "zenity --text-info --filename=/tmp/pirate_file.txt --editable --checkbox=CHECKBOX"
    * Item "None" "text" has text "12345" in "zenity"
    * Key combo: "<Ctrl><A>"
    * Type text: "67890"
    * Item "None" "text" has text "67890" in "zenity"
    * Item "CHECKBOX" "check box" is "showing" in "zenity"
    * Remove created file
