@list_feature
Feature: Zenity List tests


  @list_general_size
  Scenario: List set size of the widget
    * Start application "zenity" via command "zenity --list --column=NUMBER 5 Yes --width=350 --height=350"
    * Verify height and width of the widget


  @list_general_rename_ok_button
  Scenario: List rename OK button
    * Start application "zenity" via command "zenity --list --column=NUMBER 5 Yes --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @list_general_rename_cancel_button
  Scenario: List rename Cancel button
    * Start application "zenity" via command "zenity --list --column=NUMBER 5 Yes --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @list_general_add_extra_button
  Scenario: List add Extra button
    * Start application "zenity" via command "zenity --list --column=NUMBER 5 Yes --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --list --column=NUMBER 5 Yes --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @list_general_modal
  Scenario: List modal
    * Start application "zenity" via command "zenity --list --column=NUMBER 5 Yes --modal"
    * Application "zenity" is running


  @list_general_ok
  Scenario: List OK
    * Start application "zenity" via command "zenity --list --column=BIGENOUGH --column=NUMBER Yes 5 No 10 --checklist"
    * Wait 1 second before action
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Result is equal to "5|10"


  @list_general_cancel
  Scenario: List Cancel
    * Start application "zenity" via command "zenity --list --column=BIGENOUGH --column=NUMBER Yes 5 No 10 --checklist"
    * Wait 1 second before action
    * Select all checkboxes
    * Left click "Cancel" "push button" in "zenity"
    * Result is not equal to "5|10"


  @list_general_escape
  Scenario: List close by Esc key
    * Start application "zenity" via command "zenity --list --column=BIGENOUGH --column=NUMBER Yes 5 No 10 --checklist"
    * Wait 1 second before action
    * Select all checkboxes
    * Press key: "Esc"
    * Result is not equal to "5|10"


  @list_general_timeout
  Scenario: List Timeout
    * Start application "zenity" via command "zenity --list --column=BIGENOUGH --column=NUMBER Yes 5 No 10 --checklist --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @list
  Scenario: List
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --column=NUMBER --column=BIGENOUGH 1 No 900000 Yes"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | 1         | table cell          |
      | No        | table cell          |
      | 900000    | table cell          |
      | Yes       | table cell          |


  @list_checklist
  Scenario: List Checklist
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --checklist --column=NUMBER --column=BIGENOUGH 'False' No 'False' Yes"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "No|Yes"


  @list_radiolist
  Scenario: List Radio
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --radiolist --column=NUMBER --column=BIGENOUGH 1 No 1 Yes"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "Yes"


  @list_separator
  Scenario: List Separator
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --checklist --column=NUMBER --column=BIGENOUGH 'False' No 'False' Yes --separator=*"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "No*Yes"


  @list_print_single_column
  Scenario: List Single Column
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --checklist --column=NUMBER --column=BIGENOUGH --column=HELLO 1 No YES 1 Yes NO"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | HELLO     | table column header |
      | NO        | table cell          |
      | YES       | table cell          |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "No|Yes"


  @list_print_multiple_columns
  Scenario: List Multiple Columns
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --checklist --column=NUMBER --column=BIGENOUGH --column=HELLO 1 No YES 1 Yes NO --print-column=ALL"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | HELLO     | table column header |
      | NO        | table cell          |
      | YES       | table cell          |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "No|YES|Yes|NO"


  @list_print_single_column_separator
  Scenario: List Single Column Separator
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --checklist --column=NUMBER --column=BIGENOUGH --column=HELLO 1 No YES 1 Yes NO --separator=*"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | HELLO     | table column header |
      | NO        | table cell          |
      | YES       | table cell          |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "No*Yes"


  @list_print_multiple_columns_separator
  Scenario: List Multiple Columns Separator
    * Start application "zenity" via command "zenity --list --title=PIRATE_FLAG --checklist --column=NUMBER --column=BIGENOUGH --column=HELLO 1 No YES 1 Yes NO --print-column=ALL --separator=*"
    * Item "PIRATE_FLAG" "dialog" is "showing" in "zenity"
    * Zenity has all expected data
      | Field     | Type                |
      | NUMBER    | table column header |
      | BIGENOUGH | table column header |
      | HELLO     | table column header |
      | NO        | table cell          |
      | YES       | table cell          |
      | No        | table cell          |
      | Yes       | table cell          |
    * Select all checkboxes
    * Left click "OK" "push button" in "zenity"
    * Compare result with expected values: "No*YES*Yes*NO"
