@color_feature
Feature: Zenity Color tests


  @color_general_title
  Scenario: Color set title
    * Start application "zenity" via command "zenity --color-selection --title=PIRATE_COLOR"
    * Item "PIRATE_COLOR" "dialog" is "showing" in "zenity"


  @rhbz1788453
  @color_general_rename_ok_button
  Scenario: Color rename OK button
    * Start application "zenity" via command "zenity --color-selection --ok-label=PIRATE_OK"
    * Item "PIRATE_OK" "push button" is "showing" in "zenity"


  @rhbz1788453
  @color_general_rename_cancel_button
  Scenario: Color rename Cancel button
    * Start application "zenity" via command "zenity --color-selection --cancel-label=PIRATE_CANCEL"
    * Item "PIRATE_CANCEL" "push button" is "showing" in "zenity"


  @color_general_add_extra_button
  Scenario: Color add Extra button
    * Start application "zenity" via command "zenity --color-selection --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --color-selection --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @color_general_modal
  Scenario: Color modal
    * Start application "zenity" via command "zenity --color-selection --modal"
    * Application "zenity" is running


  @color_general_select
  Scenario: Color Select
    * Start application "zenity" via command "zenity --color-selection --color=#FF2554"
    * Item "Color Name" "text" has text "#FF2554" in "zenity"
    * Left click "Select" "push button"
    * Result is equal to "rgb(255,37,84)"


  @color_general_cancel
  Scenario: Color Cancel
    * Start application "zenity" via command "zenity --color-selection --color=#FF2554"
    * Item "Color Name" "text" has text "#FF2554" in "zenity"
    * Left click "Cancel" "push button"
    * Result is not equal to "rgb(255,37,84)"


  @color_general_escape
  Scenario: Color Escape
    * Start application "zenity" via command "zenity --color-selection --color=#FF2554"
    * Item "Color Name" "text" has text "#FF2554" in "zenity"
    * Press key: "Esc"
    * Result is not equal to "rgb(255,37,84)"


  @color_general_timeout
  Scenario: Color Timeout
    * Start application "zenity" via command "zenity --color-selection --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @color_value_in_hex
  Scenario: Color Value
    * Start application "zenity" via command "zenity --color-selection --color=#FF2554"
    * Item "Color Name" "text" has text "#FF2554" in "zenity"


  @color_value_in_rgb
  Scenario: Color Value
    * Start application "zenity" via command "zenity --color-selection --color='rgb(33,33,33)'"
    * Item "Color Name" "text" has text "#212121" in "zenity"


  @color_palette
  Scenario: Color Palette
    * Start application "zenity" via command "zenity --color-selection --show-palette"
    * Item "White" "radio button" is "showing" in "zenity"
    * Item "Black" "radio button" is "showing" in "zenity"
    * Item "Orange" "radio button" is "showing" in "zenity"


  @color_palette_general_title
  Scenario: Color Palette title
    * Start application "zenity" via command "zenity --color-selection --show-palette --title=PIRATE_PALLETE"
    * Item "White" "radio button" is "showing" in "zenity"
    * Item "Black" "radio button" is "showing" in "zenity"
    * Item "Orange" "radio button" is "showing" in "zenity"
    * Item "PIRATE_PALLETE" "dialog" is "showing" in "zenity"


  @color_palette_general_add_extra_button
  Scenario: Color Palette add Extra button
    * Start application "zenity" via command "zenity --color-selection --show-palette --extra-button=EXTRABUTTON"
    * Item "EXTRABUTTON" "push button" is "showing" in "zenity"
    * Start application "zenity" via command "zenity --color-selection --show-palette --extra-button=EXTRA1 --extra-button=EXTRA2"
    * Item "EXTRA1" "push button" is "showing" in "zenity"
    * Item "EXTRA2" "push button" is "showing" in "zenity"


  @color_palette_general_modal
  Scenario: Color Palette modal
    * Start application "zenity" via command "zenity --color-selection --show-palette --modal"
    * Application "zenity" is running


  @color_palette_select
  Scenario: Color Palette Select
    * Start application "zenity" via command "zenity --color-selection --show-palette"
    * Left click "Black" "radio button"
    * Left click "Select" "push button"
    * Result is equal to "rgb(0,0,0)"


  @color_palette_cancel
  Scenario: Color Palette Cancel
    * Start application "zenity" via command "zenity --color-selection --show-palette"
    * Left click "Black" "radio button"
    * Left click "Cancel" "push button"
    * Result is not equal to "rgb(0,0,0)"


  @color_palette_escape
  Scenario: Color Palette Escape
    * Start application "zenity" via command "zenity --color-selection --show-palette"
    * Left click "Black" "radio button"
    * Press key: "Esc"
    * Result is not equal to "rgb(0,0,0)"