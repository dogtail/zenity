@notification_feature
Feature: Zenity Notification tests


  @notification_unparsable
  Scenario: Notification Frame
    * Run zenity with unparsable notification text and verify error message


  @notification
  Scenario: Notification Frame
    * Set notification to appear
    * Run command: "zenity --notification --text=PIRATE_TEST"
    * Item "PIRATE_TEST" "label" is "visible" in "gnome-shell"
    * Set notification to not appear
