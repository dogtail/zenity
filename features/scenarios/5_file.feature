@file_feature
Feature: Zenity File tests


  @file_selection_general_title
  Scenario: File Selection general Title
    * Start application "zenity" via command "zenity --file-selection --title=PIRATE_FILE"
    * File chooser dialog with PIRATE_FILE label is showing


  @file_selection_general_modal
  Scenario: File Selection modal
    * Start application "zenity" via command "zenity --file-selection --modal"
    * Application "zenity" is running


  @file_selection_general_ok
  Scenario: File Selection OK
    * Start application "zenity" via command "zenity --file-selection --filename=/tmp/test.txt"
    * Left click "OK" "push button"
    * Result is equal to "/tmp/test.txt"


  @file_selection_general_cancel
  Scenario: File Selection Cancel
    * Start application "zenity" via command "zenity --file-selection --filename=/tmp/test.txt"
    * Left click "Cancel" "push button"
    * Result is not equal to "/tmp/test.txt"


  @file_selection_general_escape
  Scenario: File Selection Escape
    * Start application "zenity" via command "zenity --file-selection --filename=/tmp/test.txt"
    * Press key: "Esc"
    * Result is not equal to "/tmp/test.txt"


  @rhbz1788497
  @file_selection_general_timeout
  Scenario: File Selection Timeout
    * Start application "zenity" via command "zenity --file-selection --timeout=1"
    * Wait 3 seconds before action
    * Application "zenity" is no longer running


  @gate
  @file_selection
  Scenario: File Selection
    * Start application "zenity" via command "zenity --file-selection"
    * Item "Recent" "label" is "showing" in "zenity"
    * Item "Home" "label" is "showing" in "zenity"


  @file_selection_filename
  Scenario: File Directory Selection
    * Start application "zenity" via command "zenity --file-selection --filename=/home/"
    * Item "test" "table cell" is "showing" in "zenity"


  @file_selection_directory_lock
  Scenario: File Directory Lock
    * Start application "zenity" via command "zenity --file-selection --filename=/home --directory"
    * Files in other directories are unselectable
