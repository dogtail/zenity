#!/usr/bin/env python3
from subprocess import Popen
from behave import step
from dogtail.rawinput import dragWithTrajectory, pressKey, typeText, doubleClick
from qecore import run
from qecore.common_steps import *


@step('Set notification to appear')
def set_notification_to_appear(context):
    run("gsettings set org.gnome.desktop.notifications show-banners true")


@step('Set notification to not appear')
def set_notification_to_disappear(context):
    run("gsettings set org.gnome.desktop.notifications show-banners false")


@step('Run zenity with unparsable notification text and verify error message')
def run_zenity_with_unparsable_notification_text_and_verify_error_message(context):
    result = run("zenity --notification --text=").strip("\n")
    assert result == "Could not parse message",\
        f"Expected error message is 'Could not parse message'\nInstead got '{result}'"


@step('Run zenity scale with min-value larger than max-value and verify error message')
def run_zenity_with_min_value_larger_than_max_value_verify_error_message(context):
    result = run("zenity --scale --min-value=5 --max-value=3").strip("\n")
    assert "Maximum value must be greater than minimum value." in result,\
        f"Expected error message is 'Maximum value must be greater than minimum value.'\nInstead got '{result}'"


@step('Run zenity scale value out of bounds and verify error message')
def run_zenity_with_scale_value_out_of_bounds_verify_error_message(context):
    result = run("zenity --scale --min-value=3 --max-value=5 --value=1").strip("\n")
    assert "Value out of range." in result,\
        f"Expected error message is 'Value out of range.'\nInstead got '{result}'"
    result = run("zenity --scale --min-value=3 --max-value=5 --value=7").strip("\n")
    assert "Value out of range." in result,\
        f"Expected error message is 'Value out of range.'\nInstead got '{result}'"



@step('Run command: "{command_to_run}"')
def run_copmmand(context, command_to_run):
    run(command_to_run)


@step('Type: "{typing_text}"')
def typing(context, typing_text):
    typeText(typing_text)


@step('Compare result from zenity with result of command: "{command}"')
def compere_results_with_result_of_command(context, command):
    if not hasattr(context, "zenity_result"):
        context.zenity_result, _ = context.zenity.process.communicate()
    result = run(command)
    assert result.strip("\n") in context.zenity_result.decode("utf-8"),\
        f"\nResult from zenity: {context.zenity_result}\nResult from command: {result}"


@step('Compare result with expected values: "{expected_value}"')
def compere_results_with_expected_values(context, expected_value):
    sleep(5)
    context.zenity_result, _ = context.zenity.process.communicate()
    result = context.zenity_result.decode("utf-8").strip("\n")
    assert expected_value in result, \
        f"\nValue expected: {expected_value}\nValue recieved: {result}"


@step('Result is equal to "{expected_value}"')
def result_is_equal_to(context, expected_value):
    sleep(3)
    context.zenity_result, _ = context.zenity.process.communicate()
    result = context.zenity_result.decode("utf-8").strip("\n")
    if expected_value == "NONE":
        assert result == "", f"Result was expected not empty but got '{result}'"
    else:
        assert expected_value in result, \
            f"\nValue expected: '{expected_value}'\nValue recieved: '{result}'"


@step('Result is not equal to "{expected_value}"')
def result_is_not_equal_to(context, expected_value):
    sleep(3)
    context.zenity_result, _ = context.zenity.process.communicate()
    result = context.zenity_result.decode("utf-8").strip("\n")
    if expected_value == "NONE":
        assert result != "", f"Result was expected not empty but got '{result}'"
    else:
        assert expected_value not in result, \
            f"\nValue expected: '{expected_value}'\nValue recieved: '{result}'"


@step('Double click calendar')
def double_click_calendar(context):
    position = context.zenity.instance.child(name="", roleName="calendar").position
    size = context.zenity.instance.child(name="", roleName="calendar").size
    calendar_center = (position[0] + int(size[0]/2), position[1] + int(size[1]/2))
    doubleClick(*(calendar_center), 1)


@step('Select all checkboxes')
def select_all_checkboxes(context):
    cells = context.zenity.instance.findChildren(lambda x:\
        x.name == "" and x.roleName == "table cell")
    for cell in cells:
        cell.click()


@step('Zenity has all expected data')
def zenity_has_all_expected_data(context):
    for row in context.table:
        name = row["Field"]
        role_name = row["Type"]
        context.execute_steps(f'* Item "{name}" "{role_name}" is "visible" in "zenity"')


@step('Make a pipe: "{new_pipe}"')
def make_pipe(context, new_pipe):
    run(f"rm /tmp/{new_pipe}; mkfifo /tmp/{new_pipe}")


@step('Listen to a pipe: "{pipe}", with: "{command}"')
def read_pipe(context, pipe, command):
    command = f"tail -f /tmp/{pipe} | {command}"
    context.process = Popen(command, shell=True)
    context.zenity.already_running()


@step('Send: "{value}", to a pipe: "{pipe}"')
def send_to_pipe(context, value, pipe):
    run(f"echo {value} > /tmp/{pipe}")

@step('Drag the slider to the "{place}"')
def drag_slider(context, place):
    slider = context.zenity.instance.findChild(lambda x:\
        x.roleName == "slider" and x.showing)

    position = slider.position
    if place == "beggining":
        dragWithTrajectory(
            (position[0] + int(slider.size[0]/2), position[1] + int(slider.size[1]/2)),
            (0, position[1] + int(slider.size[1]/2))
        )
    elif place == "end":
        dragWithTrajectory(
            (position[0] + int(slider.size[0]/2), position[1] + int(slider.size[1]/2)),
            (position[0] + int(slider.size[0]) + 5, position[1] + int(slider.size[1]/2))
        )


@step('Create file "{filename}" with data "{data}"')
def create_file(context, filename, data):
    context.file_created = f"/tmp/{filename}"
    run(f"echo \"{data}\" > /tmp/{filename}")


@step('Remove created file')
def remove_file(context):
    run(f"rm {context.file_created}")


@step('Files in other directories are unselectable')
def locked_directory(context):
    if context.sandbox.session_type != "x11":
        sleep(5)

    etc_node = context.zenity.instance.child(name="etc")
    etc_node.doubleClick()

    abrt_node = context.zenity.instance.child(name="abrt").parent
    abrt_node.click()

    result = abrt_node.selected
    pressKey("Down")
    assert result == abrt_node.selected, "Files should not be selectable!"


@step('Verify height and width of the widget')
def verify_height_and_width_of_the_widget(context):
    target = context.zenity.instance.children[0].size
    assert target[1] >= 350, f"Expected height is over 350, instead got {target[1]}"
    assert target[0] == 350, f"Expected width is 300, instead got {target[0]}"


@step('File chooser dialog with PIRATE_FILE label is showing')
def file_chooser_dialog_with_label_is_showing(context):
    # using custom command since dialog is on negative position and that is not allowed in qecore
    target = context.zenity.instance.child("PIRATE_FILE", "file chooser")
    assert target.showing, "PIRATE_FILE dialog is not showing"
