#!/usr/bin/env python3
import os
import sys
import traceback
from qecore.sandbox import TestSandbox


def before_all(context):
    try:
        context.sandbox = TestSandbox("zenity")
        context.zenity = context.sandbox.get_application(name="zenity",
                                                         desktop_file_exists=False)
    except Exception as error:
        print(f"Environment error: before_all: {error}")
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def before_scenario(context, scenario):
    try:
        context.sandbox.before_scenario(context, scenario)
    except Exception as error:
        print(f"Environment error: before_scenario: {error}")
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def after_scenario(context, scenario):
    try:
        if hasattr(context, "process"):
            os.system(f"kill -9 {context.process.pid}")
        context.sandbox.after_scenario(context, scenario)
    except Exception as error:
        print(f"Environment error: after_scenario: {error}")
        traceback.print_exc(file=sys.stdout)
